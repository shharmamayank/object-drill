function pairs(obj) {
    let arrayOfKeysAndValue = []
    if (typeof (obj) !== "object" || Array.isArray(obj)) {
        return arrayOfKeysAndValue
    }
    else {
        for (let keys in obj) {
            arrayOfKeysAndValue.push([keys, obj[keys]]);
        }
        return arrayOfKeysAndValue
    }

}
module.exports = pairs