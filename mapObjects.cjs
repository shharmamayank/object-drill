function mapObject(obj, callback) {
    let arrayOfKeys = []
    if (typeof (obj) !== "object" || Array.isArray(obj) || !callback) {
        return arrayOfKeys
    } else {
        for (let keys in obj) {
            obj[keys] = callback(obj[keys])
        }
        return obj
    }
}
module.exports = mapObject
