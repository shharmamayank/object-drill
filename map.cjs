function mapObject(obj, callback) {
    let arr = []
    if (typeof (obj) !== "object" || Array.isArray(obj) || !callback) {
        return arr
    } else {
        for (let keys in obj) {
            obj[keys] = callback(obj[keys])
        }
        return obj
    }

}
module.exports = mapObject
