function invert(obj) {
    let objectOfInvertValue = {}
    if (typeof (obj) !== "object" || Array.isArray(obj)) {
        return objectOfInvertValue
    }
    else {
        for (let keys in obj) {
            objectOfInvertValue[obj[keys]] = keys
        }
        return objectOfInvertValue
    }
}
module.exports = invert