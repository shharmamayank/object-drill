function defaults(obj, defaultProps) {
   let arrayOfKeys = []
   if (typeof (obj) !== "object" || Array.isArray(obj)) {
      return arrayOfKeys
   } else {
      for (let keys in defaultProps) {
         obj[keys] = defaultProps[keys]
      }
      return obj
   }
}
module.exports = defaults